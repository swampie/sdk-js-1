var Baasbox = (function () {
    
    var instance;
    var user;
    var endPoint;
    var COOKIE_KEY = "baasbox-cookie";

    function createInstance() {
        var object = new Object("I am the Baasbox instance");
        return object;
    }

    function setCurrentUser(username, token,roles) {
        
        this.user = new Object();
        this.user.username = username;
        this.user.token = token;
        this.user.roles = roles;
        $.cookie(COOKIE_KEY, JSON.stringify(this.user));
        
    }

    function getCurrentUser() {
        //$.removeCookie(COOKIE_KEY);
        
        if ($.cookie(COOKIE_KEY)) {
            
            this.user = JSON.parse($.cookie(COOKIE_KEY));
            
        }
        
        return this.user;
        
    }
    
    return {

        appcode: "",
        pagelength: 50,
        version: "0.2",

        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        },

        setEndPoint : function (endPointURL) {
            
            console.log("testing ", endPointURL);
            var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/

            if (regexp.test(endPointURL)) {
                
                this.endPoint = endPointURL;
                
            } else {
                
                alert(endPointURL + " is not a valid URL");
                
            }
                        
        },
        
        endPoint: function () { 
            return this.endPoint;
        },

        login: function (user, pass, cb) {
            
            var url = Baasbox.endPoint + '/login'
            var req = $.post(url, {
                username: user,
                password: pass,
                appcode: Baasbox.appcode
            })
                .done(function (res) {
                    var roles = [];

                    $(res.data.user.roles).each(function(idx,r){
                        roles.push(r.name);
                    })

                    setCurrentUser(res.data.user.name, res.data['X-BB-SESSION'],roles);
                    var u = getCurrentUser()
                    console.log("current user " + u);
                    cb(u,null);
                })
                .fail(function (e) {
                    console.log("error" + e);
                    cb(null,e);
                })

        },

        createUser: function (user, pass,cb) {

            var url = Baasbox.endPoint + '/user'

            var req = $.ajax({
                url: url,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    username: user,
                    password: pass
                }),
                beforeSend: function (r) {
                    r.setRequestHeader('X-BAASBOX-APPCODE', Baasbox.appcode);
                },
                success: function (res) {

                    setCurrentUser(res.data.user.name, res.data['X-BB-SESSION']);
                    var u = getCurrentUser()
                    cb(u,null);
                },
                error: function (e) {
                    cb(null,e)
                }
            });

        },

        getCurrentUser: function(){
            return getCurrentUser();
        },

        loadCollectionWithParams: function (collection, params, callback) {

            console.log("loading collection " + collection);

            var url = Baasbox.endPoint + '/document/' + collection
            var req = $.ajax({
                url: url,
                method: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                data: params,
                beforeSend: function (r) {
                    r.setRequestHeader('X-BAASBOX-APPCODE', Baasbox.appcode);
                    
                },
                success: function (res) {
                    callback(res['data'], null);
                },
                error: function (e) {
                    callback(null, e);
                }
            });

        },

        loadCollection: function (collection, callback) {

            Baasbox.loadCollectionWithParams(collection, {
                page: 0,
                recordsPerPage: Baasbox.pagelength
            }, callback)

        },

        // only for json assets
        loadAssetData: function (asset, callback) {

            var url = Baasbox.endPoint + '/asset/' + asset + '/data'
            var req = $.ajax({
                url: url,
                method: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                beforeSend: function (r) {
                    r.setRequestHeader('X-BAASBOX-APPCODE', Baasbox.appcode);
                },
                success: function (res) {
                    callback(res['data'], null);
                },
                error: function (e) {
                    callback(null, e);
                }
            });

        },

        getImageURI: function (name, params, callback) {

            var uri = Baasbox.endPoint + '/asset/' + name;
            var r;
            
            for(var prop in params){
                var a = [];
                a.push(prop);
                a.push(params[prop]);
                r = a.join('/');
            }
            
            uri = uri.concat('/');
            uri = uri.concat(r);
            
            p = {};
            p['X-BAASBOX-APPCODE'] = Baasbox.appcode;
            var req = $.get(uri, p)
                .done(function (res) {
                    console.log("URI is " , this.url);
                    callback({"data" : this.url}, null);
                })
                .fail(function (e) {
                    console.log("error in URI " , e);
                    callback(null, e.responseText);
                })

        }

    };

})();